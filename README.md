# Dragster

Un petit dragster conçu au fablab par @AntoineSelosse et modifié par moi même pour une animation avec des jeunes. Très simple (pas d'électronique : un moteur et une pile 9V)

Engrenages de transmission de module 2 (entraxe 33mm)
Axe de trasmission : tige filetée de 4mm + écrous
Axe de roue avant : tige venant d'une baguette à souder (environ 2,5mm)